﻿using API.GoogleAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Class that represents Result classes output as json string
    /// </summary>
    public class JsonResult
    {
        /// <summary>
        /// Constructor that takes Result.Value and Result.Message as parameteters
        /// </summary>
        /// <param name="data">Value of the result</param>
        /// <param name="message">Message of the result. Message states error reason</param>
        public JsonResult(dynamic data,string message)
        {
            ///message == null means no error
            ResultCode = (message == null) ? ResultCodes.SUCCESS : ResultCodes.ERROR;
            Data = data;
            Message = message;
        }
        /// <summary>
        /// Data to jsonize
        /// </summary>
        private dynamic Data { get; set; }
        /// <summary>
        /// List of error codes
        /// </summary>
        public enum ResultCodes {
            /// <summary>
            /// Means command completed succesfully
            /// </summary>
            SUCCESS = 0,
            /// <summary>
            /// Means command completed with errors
            /// </summary>
            ERROR =-1};
        /// <summary>
        /// ResultCode meaning whether API call is succesful or in error
        /// </summary>
        private ResultCodes ResultCode { get; set; }
        /// <summary>
        /// Message describing the error
        /// </summary>
        private string Message { get; set; }
        /// <summary>
        /// Overloading string cast
        /// </summary>
        /// <param name="result">String representation of passed result object</param>
        public static implicit operator string(JsonResult result)
        {
            if (result.ResultCode == ResultCodes.SUCCESS)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(new { Data = result.Data, ResultCode = result.ResultCode });
            }
            else if (result.ResultCode == ResultCodes.ERROR)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(new { Message = result.Message, ResultCode = result.ResultCode });
            }
            else return "{}";
        }
    }
}
