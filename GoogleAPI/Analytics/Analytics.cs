﻿using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Google.Apis.Analytics.v3.Data;

namespace API.GoogleAPI
{
    /// <summary>
    /// API for Google Analytics.
    /// Provides realtime and aggregated data on selected view
    /// </summary>
    public class Analytics : GoogleClass
    {
        /// <summary>
        /// Analytics constructor
        /// </summary>
        /// <param name="clientId">Client ID of API</param>
        /// <param name="clientSecret">Secret of API</param>
        public Analytics(string clientId, string clientSecret)
        {
            if (clientId == null || clientId == "")
                throw new ArgumentNullException("ClientId", "Empty ClientId");
            if (clientSecret == null || clientSecret == "")
                throw new ArgumentNullException("ClientSecret", "Empty ClientSecret");
            this.Scopes = new[] {AnalyticsService.Scope.AnalyticsManageUsersReadonly, AnalyticsService.Scope.AnalyticsReadonly, AnalyticsService.Scope.Analytics, AnalyticsService.Scope.AnalyticsEdit };
            this.Secret = new ClientSecrets()
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            };
            this.ApplicationName = "Analytics";
        }
        /// <summary>
        /// Get realtime data on specific view. If there are many views , resource parameter can be used to specify site's domain
        /// </summary>
        /// <param name="accountName">Name of account that has the specific view</param>
        /// <param name="viewName">Name of the view the to get data from</param>
        /// <param name="address">Address of site to get the data from. If not specified first view with given view name is chosen</param>
        /// <returns>Result with realtime analytics data. Only Realtime field of AnalyticsReply is filled</returns>
        public Result<AnalyticsReply> GetRealtimeData(string accountName, string viewName, string address = null)
        {
            if (accountName == null || accountName == "")
                return new Result<AnalyticsReply>(null, "Account name not provided");
            if (viewName == null || viewName == "")
                return new Result<AnalyticsReply>(null, "View name not provided");
            ///Check whether resource is correct domain name
            if (
                address != null
                && !Regex.Match(address, "[a-z_A-Z0-9+-.]{2,}(\\.[a-z_A-Z0-9+-.]{2,}){1,}").Success
               )
                return new Result<AnalyticsReply>(null, "Address is not a correct");
            if (!IsAuthorized)
                return new Result<AnalyticsReply>(null, "Not authorized. Need to authorize first");
            try
            {
                if (address != null && address.StartsWith("www."))
                    address = address.Replace("www.", "");
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<AnalyticsReply>(null, baseClient.Message);
                var analyticsService = new AnalyticsService(baseClient.Value);
                var listAccountAction = analyticsService.Management.Accounts.List();
                var accounts = listAccountAction.Execute();
                if (!accounts.Items.Any(account => account.Name == accountName))
                    return new Result<AnalyticsReply>(null, "No such account in this user");
                var accountId = accounts.Items.First(account => account.Name == accountName).Id;
                ///retrieve all views then choose from them
                var listViewAction = analyticsService.Management.Profiles.List(accountId, "~all");
                var views = listViewAction.Execute();
                if (!views.Items.Any(view => view.Name == viewName))
                    return new Result<AnalyticsReply>(null, "No such view in this account");
                if (address != null)
                {
                    ///strip out domain from URI and check 
                    if (!views.Items.Any(view => view.Name == viewName && new Uri(view.WebsiteUrl.Replace("www.", "")).Host == address))
                        return new Result<AnalyticsReply>(null, "No such view that has given resource (domain)");
                }
                var viewId = (address == null)
                    ? views.Items.First(view => view.Name == viewName).Id
                    : views.Items.First(view => view.Name == viewName && new Uri(view.WebsiteUrl.Replace("www.", "")).Host == address).Id;
                var dataGetAction = analyticsService.Data.Realtime.Get("ga:" + viewId, "rt:activeUsers");
                ///country, city , usertype(new or existing) 
                dataGetAction.Dimensions = "rt:country,rt:city,rt:userType";
                var data = dataGetAction.Execute();
                var analyticsReply = _ParseAnalyticsRealtime(data);
                return new Result<AnalyticsReply>(analyticsReply);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<AnalyticsReply>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<AnalyticsReply>(null, "Unhandled exception occured", e);
            }
        }
        /// <summary>
        /// Parses Google Realtime Data to our format
        /// </summary>
        /// <param name="data">Data from Google</param>
        /// <returns>AnalyticReply filled with Google RealtimeData</returns>
        private AnalyticsReply _ParseAnalyticsRealtime(RealtimeData data)
        {
            if (data == null)
                throw new ArgumentNullException("Data parameter was null");
            var analyticsReply = new AnalyticsReply(true);
            analyticsReply.Realtime.RealtimeTotalUsers = int.Parse(data.TotalsForAllResults["rt:activeUsers"]);
            ///traverse each row and add to list
            if (data.Rows != null)
            {
                foreach (var row in data.Rows)
                {
                    ///"rt:country,rt:city,rt:userType"
                    var analyticsRow = new AnalyticsReplyHelper.AnalyticsReplyRow()
                    {
                        Country = row[0],
                        City = row[1],
                        UserType = row[2],
                        Count = int.Parse(row[3])
                    };
                    analyticsReply.Realtime.Rows.Add(analyticsRow);
                }
            }
            return analyticsReply;
        }
        /// <summary>
        /// Gets aggregate data for each day between selected periods for given analytics view
        /// </summary>
        /// <param name="accountName">Name of account to get data from</param>
        /// <param name="viewName">Name of view to retrieve data from</param>
        /// <param name="fromDate">Start date of analytics data</param>
        /// <param name="toDate">End date of analytics data</param>
        /// <param name="address">Address of site. Set it if there is more than one view with same name</param>
        /// <returns>Result with aggregate analytics data. Only Ga field of AnalyticsReply is filled</returns>
        public Result<AnalyticsReply> GetDataForPeriod(string accountName, string viewName, DateTime fromDate, DateTime toDate, string address = null)
        {
            if (accountName == null || accountName == "")
                return new Result<AnalyticsReply>(null, "Account name not provided");
            if (viewName == null || viewName == "")
                return new Result<AnalyticsReply>(null, "View name not provided");
            if (fromDate == null)
                return new Result<AnalyticsReply>(null, "From date not provided");
            if (toDate == null)
                return new Result<AnalyticsReply>(null, "To date not provided");
            if (fromDate > toDate)
                return new Result<AnalyticsReply>(null, "To date cannot be more than from date");
            ///check whether resource is valid domain
            if (
                address != null
                && !Regex.Match(address, "[a-z_A-Z0-9+-.]{2,}(\\.[a-z_A-Z0-9+-.]{2,}){1,}").Success
               )
                return new Result<AnalyticsReply>(null, "Resource is not a correct domain");
            if (!IsAuthorized)
                return new Result<AnalyticsReply>(null, "Not authorized. Need to authorize first");
            try
            {
                if (address != null && address.StartsWith("www."))
                    address = address.Replace("www.", "");
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<AnalyticsReply>(null, baseClient.Message);
                var analyticsService = new AnalyticsService(baseClient.Value);
                var listAccountAction = analyticsService.Management.Accounts.List();
                var accounts = listAccountAction.Execute();
                if (!accounts.Items.Any(account => account.Name == accountName))
                    return new Result<AnalyticsReply>(null, "No such account in this user");
                var accountId = accounts.Items.First(account => account.Name == accountName).Id;
                var listViewAction = analyticsService.Management.Profiles.List(accountId, "~all");
                var views = listViewAction.Execute();
                if (!views.Items.Any(view => view.Name == viewName))
                    return new Result<AnalyticsReply>(null, "No such view in this account");
                if (address != null)
                {
                    if (!views.Items.Any(view => view.Name == viewName && new Uri(view.WebsiteUrl.Replace("www.", "")).Host == address))
                        return new Result<AnalyticsReply>(null, "No such view that has given resource (domain)");
                }
                var viewId = (address == null)
                    ? views.Items.First(view => view.Name == viewName).Id
                    : views.Items.First(view => view.Name == viewName && new Uri(view.WebsiteUrl.Replace("www.", "")).Host == address).Id;
                var dataGetAction = analyticsService.Data.Ga.Get("ga:" + viewId
                    , fromDate.ToString("yyyy-MM-dd"), toDate.ToString("yyyy-MM-dd"), "ga:users,ga:pageviews");
                dataGetAction.Dimensions = "ga:date,ga:pagePath,ga:country,ga:city,ga:browser,ga:userType";
                dataGetAction.SamplingLevel = DataResource.GaResource.GetRequest.SamplingLevelEnum.HIGHERPRECISION;
                dataGetAction.Sort = "ga:date";
                var data = dataGetAction.Execute();
                var analyticsReply = _ParseAnalyticsAggregate(data);
                return new Result<AnalyticsReply>(analyticsReply);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<AnalyticsReply>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<AnalyticsReply>(null, "Unhandled exception occured", e);
            }
        }
        /// <summary>
        /// Parses and returns AnalyticsReply with Ga field filled
        /// </summary>
        /// <param name="data">Google GaData</param>
        /// <returns>Analytics reply parsed from Google GaData</returns>
        private AnalyticsReply _ParseAnalyticsAggregate(GaData data)
        {
            if (data == null)
                throw new ArgumentNullException("Data parameter was null");
            var analyticsReply = new AnalyticsReply(false);
            analyticsReply.Ga.TotalUsers = int.Parse(data.TotalsForAllResults["ga:users"]);
            analyticsReply.Ga.TotalPageViews = int.Parse(data.TotalsForAllResults["ga:pageviews"]);
            if (data.Rows != null)
            {
                foreach (var row in data.Rows)
                {
                    ///"ga:date,ga:pagePath,ga:country,ga:city,ga:browser,ga:users,ga:pageviews,ga:userType"
                    ///Google sends date in yyyyMMdd format
                    var analyticsRow = new AnalyticsReplyHelper.AnalyticsReplyRowDetailed()
                    {
                        Date = DateTime.ParseExact(row[0], "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture),
                        PagePath = row[1],
                        Country = row[2],
                        City = row[3],
                        Browser = row[4],
                        UserType = row[5],
                        Users = int.Parse(row[6]),
                        PageViews = int.Parse(row[7]),
                    };
                    analyticsReply.Ga.Rows.Add(analyticsRow);
                }
            }
            return analyticsReply;
        }
    }
}
