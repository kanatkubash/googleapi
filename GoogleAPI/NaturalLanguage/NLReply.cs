﻿using API.GoogleAPI.NLReplyHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Natural Language results are converted to this class
    /// </summary>
    public class NLReply
    {
        /// <summary>
        /// Sentiment analysis data if present
        /// </summary>
        public NLSentiment SentimentData { get; set; }
        /// <summary>
        /// Entity analysis data if present
        /// </summary>
        public List<NLEntity> EntityData { get; set; }
    }

}