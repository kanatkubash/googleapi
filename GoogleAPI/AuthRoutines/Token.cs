﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
namespace API.GoogleAPI.Auth
{
    /// <summary>
    /// This class represents OAuth access and refresh tokens from Google
    /// Also its model to SQLITE net 
    /// </summary>
    class Token
    {
        /// <summary>
        /// Id of the bot api
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Access token from Google Oauth Service
        /// </summary>
        [MaxLength(100)]
        public string AccessToken { get; set; }
        /// <summary>
        /// Refresh token to get new access token from Google Oauth
        /// </summary>
        [MaxLength(100)]
        public string RefreshToken { get; set; }
        /// <summary>
        /// Issue date of access token to check whether new access token is needed
        /// </summary>
        public DateTime IssueDate { get; set; }
        /// <summary>
        /// Name of the application that uses this token. Used to separate app scopes 
        /// </summary>
        public string ApplicationName { get; set; }
        /// <summary>
        /// Expire date of access token in Datetime.
        /// </summary>
        public DateTime ExpireDate { get; set; }
        /// <summary>
        /// Expire time in seconds
        /// </summary>
        public int ExpireTime { get; set; }
    }
}
