﻿///TODO: use app name as user in all 
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Base class to handle login routines
    /// </summary>
    public abstract class GoogleClass
    {
        /// <summary>
        /// Exception that is thrown when authorization required to get token
        /// </summary>
        internal class AuthorizationRequiredException : Exception { };
        #region FIELDS
        /// <summary>
        /// List of scopes used with this API
        /// </summary>
        public IEnumerable<string> Scopes { get; set; }
        /// <summary>
        /// Name of the API (name of application). Used to get token. Used only with OAuth
        /// </summary>
        public string ApplicationName { get; set; }
        /// <summary>
        /// Api Key to access Google APIs
        /// </summary>
        protected string ApiKey { get; set; }
        /// <summary>
        /// OAuth client secret
        /// </summary>
        protected ClientSecrets Secret { get; set; }
        /// <summary>
        /// Shows whether user authorized to provide user data. If not, AuthorizeForId method needs to be called
        /// </summary>
        public bool IsAuthorized { get; protected set; } = false;
        /// <summary>
        /// Tokens provider. Can be reconfigured to get data from MySQL, file etc. by implementing it
        /// </summary>
        Auth.TokenProvider TokenSource { get; set; }
        /// <summary>
        /// Id of the calling bot
        /// </summary>
        public int Id { get; private set; }
        /// <summary>
        /// OAuth token.Used internally
        /// </summary>
        private Auth.Token Token { get; set; }
        /// <summary>
        /// Authorization requests and access token renewing is handled by Authorizer. Only one instance should be instansiated
        /// </summary>
        private static Auth.WebAuthorizer Authorizer { get; set; }
        /// <summary>
        /// Domain Name this server is running
        /// </summary>
        public static string DomainName { get; set; }
        #endregion
        #region METHODS
        /// <summary>
        /// Authorizes API calls for given user id. User id is likely bots id.
        /// </summary>
        /// <param name="id">Id of the bot</param>
        /// <returns>True if succesful, false if authorizing is not succesful 
        /// and string (url) if user authorization through browser is required</returns>
        public Result<object> AuthorizeForId(int id)
        {
            if (id < 0)
                return new Result<object>(false, "Negative id provided");
            this.Id = id;
            var result = TokenSource.GetTokenForId(Id, ApplicationName);
            if (result.Value != null)
            {
                Token = result.Value;
                IsAuthorized = true;
                return new Result<object>(true);
            }
            else
            {
                ///this piece of code means we havent found any token with given id
                if (result.Message == null)
                {
                    ///we generate uri to perform oauth and send it
                    var urlResult = Authorizer.GetUrlForAuthorization(Secret, Scopes, SetToken);
                    if (urlResult.Value != null)
                    {
                        return new Result<object>(urlResult.Value);
                    }
                    else
                        return new Result<object>(false, urlResult.Message);
                }
                else
                    return new Result<object>(false, result.Message);
            }
        }
        /// <summary>
        /// Base contructor that does initialize tokenprovider
        /// </summary>
        protected GoogleClass()
        {
            ///using default token source. Note that if second parameter is not given, DB is created newly
            TokenSource = new Auth.TokenProviderSqlite("tokenstore.sqlite", true);
            if (DomainName == null || DomainName == "")
                throw new ArgumentException("Domain not set", "DomainName");
            if (Authorizer == null)
                Authorizer = Auth.WebAuthorizer.Create(DomainName);
        }
        /// <summary>
        /// Gets base service initializer to use with API services
        /// </summary>
        /// <returns>Result with value set to BaseServiceInitializer if succesful. Otherwise message is given with error</returns>
        protected Result<BaseClientService.Initializer> GetBaseServiceInitializer()
        {
            ///do token check
            if (this.ApiKey != null)
            {
                var baseServiceInitializer = new BaseClientService.Initializer()
                {
                    ApiKey = this.ApiKey
                };
                return new Result<BaseClientService.Initializer>(baseServiceInitializer);
            }
            else if (this.Secret != null)
            {
                ///Create token manually
                var t = new TokenResponse()
                {
                    AccessToken = Token.AccessToken,
                    ExpiresInSeconds = Token.ExpireTime,
                    Issued = Token.IssueDate,
                    RefreshToken = Token.RefreshToken,
                    TokenType = "Bearer",
                    Scope = this.Scopes.Aggregate((a, b) => a + " " + b).Trim()
                };
                UserCredential user = new UserCredential(
                    new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer() { ClientSecrets = Secret }), "user", t);
                var baseServiceInitializer = new BaseClientService.Initializer()
                {
                    ApiKey = this.ApiKey,
                    HttpClientInitializer = user
                };
                return new Result<BaseClientService.Initializer>(baseServiceInitializer);
            }
            else
            {
                return new Result<BaseClientService.Initializer>(null, "Neither API Key nor ClientID was provided");
            }
        }
        /// <summary>
        /// This function is used as callback for web authorizer. Its called when authorizer gets token for specific id
        /// </summary>
        /// <param name="token">Token that WebAuthorizer obtained</param>
        private void SetToken(Auth.Token token)
        {
            token.Id = this.Id;
            token.ApplicationName = this.ApplicationName;
            TokenSource.SaveTokenForId(this.Id, token);
            this.IsAuthorized = true;
            this.Token = token;
        }
        #endregion
    }
}
