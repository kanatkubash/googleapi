﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Class that provides youtube video data 
    /// </summary>
    public class YoutubeReply
    {
        /// <summary>
        /// Id of the video. Id can be concatenated to watch?={id} uri to view from browser
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Title of the video
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Publish date of the video
        /// </summary>
        public DateTime PublishDate { get; set; }
    }
}
