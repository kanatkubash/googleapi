﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Calendar.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3.Data;

namespace API.GoogleAPI
{
    /// <summary>
    /// Calendar API that provides methods for setting , moving meetings and checking for availability of given dates
    /// </summary>
    public class Calendar : GoogleClass
    {
        /// <summary>
        /// Constructor of Calendar API
        /// </summary>
        /// <param name="clientId">Client id of API</param>
        /// <param name="clientSecret">Client secret of API</param>
        public Calendar(string clientId, string clientSecret)
        {
            if (clientId == null || clientId == "")
                throw new ArgumentNullException("ClientId", "Empty ClientId");
            if (clientSecret == null || clientSecret == "")
                throw new ArgumentNullException("ClientSecret", "Empty ClientSecret");
            ///define scopes
            this.Scopes = new[] { CalendarService.Scope.Calendar };
            this.Secret = new ClientSecrets()
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            };
            this.ApplicationName = "Calendar";
        }
        /// <summary>
        /// Method that sets meeting on calendar on given dates
        /// </summary>
        /// <param name="calendarName">Name of calendar to set meeting on</param>
        /// <param name="meetingName">Name of the meeting or event to be set</param>
        /// <param name="fromDate">Starting date of meeting</param>
        /// <param name="toDate">Ending date of meeting. If not set defaults to one hour</param>
        /// <returns>Result with value set to true if succesful, 
        /// otherwise Result.Value set to false and message is set explaining failure</returns>
        public Result<bool> SetMeeting(string calendarName, string meetingName, DateTime fromDate, DateTime? toDate = null)
        {
            if (calendarName == null || calendarName == "")
                return new Result<bool>(false, "Calendar name not provided");
            if (meetingName == null || meetingName == "")
                return new Result<bool>(false, "Meeting name not provided");
            if (toDate != null && toDate <= fromDate)
                return new Result<bool>(false, "To date should be after from date");
            try
            {
                ///Default event time is one hour
                if (toDate == null)
                    toDate = fromDate + TimeSpan.FromHours(1);
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<bool>(false, baseClient.Message);
                var calendarService = new Google.Apis.Calendar.v3.CalendarService(baseClient.Value);
                var calendarListAction = calendarService.CalendarList.List();
                var calendarList = calendarListAction.Execute();
                if (!calendarList.Items.Any(calendarItem => calendarItem.Summary == calendarName))
                    return new Result<bool>(false, "No calendar with given name exists");
                var calendarId = calendarList.Items.First(calendarItem => calendarItem.Summary == calendarName).Id;
                var eventsListAction = calendarService.Events.List(calendarId);
                eventsListAction.TimeMin = fromDate;
                eventsListAction.TimeMax = toDate;
                ///Setting max result to 1, as it's not needed to list them
                ///Just checking whether this time period is free
                eventsListAction.MaxResults = 1;
                var eventList = eventsListAction.Execute();
                if (eventList.Items.Count > 0)
                {
                    return new Result<bool>(false, "Selected period is not vacant");
                }
                else
                {
                    var meeting = new Event()
                    {
                        Start = new EventDateTime() { DateTime = fromDate },
                        End = new EventDateTime() { DateTime = toDate },
                        Summary = meetingName,
                        ///Red color
                        ColorId = "11"
                    };
                    var insertAction = calendarService.Events.Insert(meeting, calendarId);
                    insertAction.Execute();
                    return new Result<bool>(true);
                }
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<bool>(false, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<bool>(false, "Unhandled exception", e);
            }
        }
        /// <summary>
        /// Check availability of given dates
        /// </summary>
        /// <param name="calendarName">Name of calendar to check for</param>
        /// <param name="fromDate">Start date to check</param>
        /// <param name="toDate">End date to check</param>
        /// <returns>If vacant, Result with Value set to true returned
        /// otherwise, Result with Value set to false and message returned</returns>
        public Result<bool> CheckAvailability(string calendarName, DateTime fromDate, DateTime toDate)
        {
            if (calendarName == null || calendarName == "")
                return new Result<bool>(false, "Calendar name not provided");
            if (toDate <= fromDate)
                return new Result<bool>(false, "To date should be after from date");
            if (!IsAuthorized)
                return new Result<bool>(false, "Not authorized. Need to authorize first");
            try
            {
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<bool>(false, baseClient.Message);
                var calendarService = new Google.Apis.Calendar.v3.CalendarService(baseClient.Value);
                var calendarListAction = calendarService.CalendarList.List();
                var calendarList = calendarListAction.Execute();
                if (!calendarList.Items.Any(calendarItem => calendarItem.Summary == calendarName))
                    return new Result<bool>(false, "No calendar with given name exists");
                var calendarId = calendarList.Items.First(calendarItem => calendarItem.Summary == calendarName).Id;
                var eventsListAction = calendarService.Events.List(calendarId);
                eventsListAction.TimeMin = fromDate;
                eventsListAction.TimeMax = toDate;
                ///Setting max result to 1, as it's not needed to list them
                ///Just checking whether this time period is free
                eventsListAction.MaxResults = 1;
                var eventList = eventsListAction.Execute();
                if (eventList.Items.Count > 0)
                    return new Result<bool>(false);
                else
                    return new Result<bool>(true);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<bool>(false, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<bool>(false, "Unhandled exception", e);
            }
        }
        /// <summary>
        /// Moves meeting with given name on given calendar to another dates
        /// </summary>
        /// <param name="calendarName">Name of calendar where meeting resides</param>
        /// <param name="meetingName">Name of the meeting to move</param>
        /// <param name="newFromDate">New starting date to move the event</param>
        /// <param name="newToDate">New end date of event. If not set, defaults to one hour starting from NewFromDate</param>
        /// <returns>Result with Value=true if method succeeds 
        /// otherwise, Result with=Value=false and Message is returned</returns>
        public Result<bool> MoveMeeting(string calendarName, string meetingName, DateTime newFromDate, DateTime? newToDate = null)
        {
            if (calendarName == null || calendarName == "")
                return new Result<bool>(false, "Calendar name not provided");
            if (meetingName == null || meetingName == "")
                return new Result<bool>(false, "Meeting name not provided");
            if (newToDate != null && newToDate <= newFromDate)
                return new Result<bool>(false, "To date should be after from date");
            if (!IsAuthorized)
                return new Result<bool>(false, "Not authorized. Need to authorize first");
            try
            {
                ///Default event time is one hour
                if (newToDate == null)
                    newToDate = newFromDate + TimeSpan.FromHours(1);
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<bool>(false, baseClient.Message);
                var calendarService = new Google.Apis.Calendar.v3.CalendarService(baseClient.Value);
                var calendarListAction = calendarService.CalendarList.List();
                var calendarList = calendarListAction.Execute();
                if (!calendarList.Items.Any(calendarItem => calendarItem.Summary == calendarName))
                    return new Result<bool>(false, "No calendar with given name exists");
                var calendarId = calendarList.Items.First(calendarItem => calendarItem.Summary == calendarName).Id;
                var eventsListAction = calendarService.Events.List(calendarId);
                eventsListAction.TimeMin = newFromDate;
                eventsListAction.TimeMax = newToDate;
                ///Setting max result to 1, as it's not needed to list them
                ///Just checking whether this time period is free
                eventsListAction.MaxResults = 1;
                var eventList = eventsListAction.Execute();
                if (eventList.Items.Count > 0)
                {
                    return new Result<bool>(false,"Not vacant");
                }
                else
                {
                    eventsListAction = calendarService.Events.List(calendarId);
                    ///Filter by name of the event
                    eventsListAction.Q = meetingName;
                    eventList = eventsListAction.Execute();
                    if (eventList.Items.Count == 0)
                    {
                        return new Result<bool>(false, "No meeting with given name is found");
                    }
                    else
                    {
                        var eventId = eventList.Items.First().Id;
                        var getEventAction = calendarService.Events.Get(calendarId, eventId);
                        var alteredMeeting = getEventAction.Execute();
                        alteredMeeting.Start = new EventDateTime() { DateTime = newFromDate };
                        alteredMeeting.End = new EventDateTime() { DateTime = newToDate };
                        var updateAction = calendarService.Events.Update(alteredMeeting, calendarId, alteredMeeting.Id);
                        updateAction.Execute();
                        return new Result<bool>(true);
                    }
                }
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<bool>(false, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<bool>(false, "Unhandled exception", e);
            }
        }
    }
}
