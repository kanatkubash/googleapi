﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Class that represents method call results within API
    /// </summary>
    /// <typeparam name="T">Type of the Result</typeparam>
    public class Result<T>
    {
        /// <summary>
        /// Result class constructor from given data
        /// </summary>
        /// <param name="value">Value of the result. Filled when no error occurs</param>
        /// <param name="message">If method fails , this message field is filled</param>
        /// <param name="exception">Exception is provided when unhandled exception occured to log exception details</param>
        public Result(T value, string message = null, Exception exception = null)
        {
            this.Value = value;
            this.Message = message;
            if (exception != null)
            {
                logException(exception);
            }
        }
        /// <summary>
        /// Message why method failed
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Result of the method, Set to non-null value if method call succeeded
        /// </summary>
        public T Value { get; set; }
        /// <summary>
        /// If exception occured , exception is logged to file
        /// </summary>
        /// <param name="exception">Exception object</param>
        private static void logException(Exception exception)
        {
            File.AppendAllText("exceptions.log", exception.ToString());
        }
    }
}
