﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI.Auth
{
    /// <summary>
    /// Interface to get access token or refresh token based on ID. Interface should be implemented 
    /// using some kind of storage like DB or file
    /// </summary>
    interface TokenProvider
    {
        /// <summary>
        /// Gets Token object by ID. If not succesfult tells what's the problem by Result.Message value
        /// </summary>
        /// <param name="id">Id of bot API user</param>
        /// <param name="applicationName">Name of the application (scope)</param>
        /// <returns>Oauth Token object</returns>
        Result<Token> GetTokenForId(int id,string applicationName);
        /// <summary>
        /// Persists token for later retrieval
        /// </summary>
        /// <param name="id">Id of the bot api</param>
        /// <param name="token">Token to be saved</param>
        /// <returns>Result with Value set to true if succesful or false if something went wrong</returns>
        Result<bool> SaveTokenForId(int id,Token token);
    }
}
