﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI.NLReplyHelper
{
    /// <summary>
    /// Class to provide entity data when Entity analysis is performed
    /// </summary>
    public class NLEntity
    {
        /// <summary>
        /// Name of the Entity
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Type of the entity: Person, Location etc.
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// Text form of the entity in analyzed text
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Uri related to entity if present
        /// </summary>
        public string RelatedUri { get; set; }
        /// <summary>
        /// Salience is importance of given entity within text. Salience is measured between 0.0 and 1.0 if given
        /// </summary>
        public double? Salience { get; set; }
    }
}
