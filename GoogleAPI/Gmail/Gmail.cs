﻿///If we want to speed up message retrieving we can define parallel symbol,
///otherwise if parallel getting is not needed just comment the next line
#define parallel
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Gmail.v1.Data;
using System.Text.RegularExpressions;
using System.Collections.Concurrent;
namespace API.GoogleAPI
{
    /// <summary>
    /// Gmail API to perform getting unread messages and sending messages
    /// </summary>
    public class Gmail : GoogleClass
    {
        /// <summary>
        /// Constructor of Gmail API
        /// </summary>
        /// <param name="clientId">Client id of an API</param>
        /// <param name="clientSecret">Client secret of an API</param>
        public Gmail(string clientId, string clientSecret)
        {
            if (clientId == null || clientId == "")
                throw new ArgumentNullException("ClientId", "Empty ClientId");
            if (clientSecret == null || clientSecret == "")
                throw new ArgumentNullException("ClientSecret", "Empty ClientSecret");
            this.Scopes = new[] { GmailService.Scope.GmailSend, GmailService.Scope.GmailReadonly };
            this.Secret = new ClientSecrets()
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            };
            this.ApplicationName = "Gmail";
        }
        /// <summary>
        /// Gets unread messages from given date
        /// </summary>
        /// <param name="username">User email</param>
        /// <param name="fromDate">The date to limit the results</param>
        /// <param name="count">Amount of unread messages to get</param>
        /// <returns>Result with Value set to List of Gmail Reply items if succesful, otherwise
        /// Result.Value=false and message is given</returns>
        public Result<List<GmailReply>> GetUnreadMessages(string username, DateTime fromDate, int count = 30)
        {
            if (username == null || username == "")
                return new Result<List<GmailReply>>(null, "Username not provided");
            if (!Regex.Match(username, "[a-z_A-Z0-9+-.]{2,}@[a-z_A-Z0-9+-.]{2,}\\.[a-z_A-Z0-9+-.]{2,}").Success)
                return new Result<List<GmailReply>>(null, "Incorrect username provided. Should be email address");
            if (fromDate == null)
                return new Result<List<GmailReply>>(null, "Null date provided");
            if (count > 30 || count < 1)
                return new Result<List<GmailReply>>(null, "Count should be between 1 and 30");
            if (!IsAuthorized)
                return new Result<List<GmailReply>>(null, "Not authorized. Need to authorize first");
            try
            {
                ///Parallel method of getting gmail messages, because each message is got using Http request, Parallelizing may shorten the time needed 
                ///to get all messages
#if parallel
                ConcurrentBag<GmailReply> gmailReplyList = new ConcurrentBag<GmailReply>();
#else
                List<GmailReply> gmailReplyList = new List<GmailReply>();
#endif
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<List<GmailReply>>(null, baseClient.Message);
                var gmailService = new GmailService(baseClient.Value);
                var listMailAction = gmailService.Users.Messages.List(username);
                listMailAction.MaxResults = count;
                listMailAction.Fields = "messages(id)";
                listMailAction.Q = "is:unread date-begin:" + fromDate.ToString("yyyy-MM-dd");
                listMailAction.IncludeSpamTrash = false;
                var listResponse = listMailAction.Execute();
#if parallel
                ///parallel way of getting messages by id
                Parallel.ForEach(listResponse.Messages, (messageInList) =>
                 {
                     var getMailAction = gmailService.Users.Messages.Get(username, messageInList.Id);
                     getMailAction.Format = UsersResource.MessagesResource.GetRequest.FormatEnum.Full;
                     var message = getMailAction.Execute();
                     gmailReplyList.Add(_ParseGmailReply(message));
                 });
#else
                ///concurrent way of getting messages by id
                foreach (var messageInList in listResponse.Messages)
                {
                    var getMailAction = gmailService.Users.Messages.Get(username, messageInList.Id);
                    getMailAction.Format = UsersResource.MessagesResource.GetRequest.FormatEnum.Full;
                    var message = getMailAction.Execute();
                    gmailReplyList.Add(_ParseGmailReply(message));
                }
#endif
#if parallel
                return new Result<List<GmailReply>>(gmailReplyList.ToList());
#else
                return new Result<List<GmailReply>>(gmailReplyList);
#endif
            }
            catch (ArgumentNullException)
            {
                return new Result<List<GmailReply>>(null, "Message data or some of its part from GAPI was null. Give a retry");
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<List<GmailReply>>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<List<GmailReply>>(null, "Unhandled exception occured", e);
            }
        }
        /// <summary>
        /// Sends message to given recipient with subject and text set
        /// </summary>
        /// <param name="to">Recipients email address</param>
        /// <param name="subject">Subject of message</param>
        /// <param name="text">Text of the message</param>
        /// <returns>Result with Value=true if succeeds, otherwise
        /// Result.Value=false and message is given</returns>
        public Result<bool> SendMessage(string to, string subject, string text)
        {
            if (to == null || to == "")
                return new Result<bool>(false, "Email not provided");
            if (!Regex.Match(to, "[a-z_A-Z0-9+-.]{2,}@[a-z_A-Z0-9+-.]{2,}\\.[a-z_A-Z0-9+-.]{2,}").Success)
                return new Result<bool>(false, "Incorrect email provided");
            if (subject == null)
                return new Result<bool>(false, "Null subject provided");
            if (text == null)
                return new Result<bool>(false, "Null text provided");
            if (!IsAuthorized)
                return new Result<bool>(false, "Not authorized. Need to authorize first");
            try
            {
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<bool>(false, baseClient.Message);
                var gmailService = new GmailService(baseClient.Value);
                var from = gmailService.Users.GetProfile("me").Execute().EmailAddress;
                ///Construct GmailAPI Message Object from given values 
                var message = _ConstructMessage(to, subject, text);
                var sendAction = gmailService.Users.Messages.Send(message, "me");
                var resultMessage = sendAction.Execute();
                if (resultMessage.Id != null || resultMessage.Id != "")
                    return new Result<bool>(true);
                else
                    return new Result<bool>(false, "Message has not been sent");
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<bool>(false, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<bool>(false, "Unhandled exception", e);
            }
        }
        /// <summary>
        /// Constructs Google.Apis.Gmail Message object from given values
        /// </summary>
        /// <param name="to">Email of recipient</param>
        /// <param name="subject">Subject of message</param>
        /// <param name="text">Text of the message</param>
        /// <returns>Google.Apis.Gmail.v1.Data.Message object to pass to GmailService</returns>
        private Message _ConstructMessage(string to, string subject, string text)
        {
            var mailString = new StringBuilder();
            var keyValuePairs = new Dictionary<string, string>();
            keyValuePairs["To: "] = to;
            keyValuePairs["Subject: "] = subject;
            //keyValuePairs["Content-Type: "] = "text/html";
            foreach (var pair in keyValuePairs)
            {
                mailString.Append(pair.Key + pair.Value + "\r\n");
            }
            mailString.Append("\r\n\r\n");
            mailString.Append(text);
            var message = new Message();
            var base64String = Convert.ToBase64String(Encoding.UTF8.GetBytes(mailString.ToString()));
            ///URL safe base64-ing
            base64String = base64String.Replace('/', '_').Replace('+', '-');
            message.Raw = base64String;
            return message;
        }
        /// <summary>
        /// Conevrt Google.Apis.Gmail.v1.Data.Message object to our GmailReply format
        /// </summary>
        /// <param name="message">Google.Apis.Gmail.v1.Data.Message object to parse</param>
        /// <returns>GmailReply object</returns>
        private GmailReply _ParseGmailReply(Message message)
        {
            if (message == null || message.InternalDate == null || message.Payload == null)
                throw new ArgumentNullException();
            var gmailReply = new GmailReply();
            ///to get date from Unix Timestamp
            gmailReply.Date = (DateTime.Parse("1970-01-01") + TimeSpan.FromMilliseconds(message.InternalDate.Value)).ToLocalTime();
            gmailReply.Id = message.Id;
            gmailReply.Subject = message.Payload.Headers.First(header => header.Name == "Subject").Value;
            gmailReply.From = message.Payload.Headers.First(header => header.Name == "From").Value;
            string base64Text = "";
            ///if we find useful payload in terms of text/html or text/plain we take it 
            ///otherwise, we dig deeper for each part to get useful payload. Some messages are multipart that's why recursive search is needed
            if (message.Payload.MimeType == "text/html" || message.Payload.MimeType == "text/plain")
                base64Text = message.Payload.Body.Data;
            else
            {
                var messageParts = message.Payload.Parts;
                while (true)
                {
                    if (messageParts.Any(part => part.MimeType == "text/html"))
                    {
                        base64Text = messageParts.First(part => part.MimeType == "text/html").Body.Data;
                        break;
                    }
                    else if (messageParts.Any(part => part.MimeType == "text/plain"))
                    {
                        base64Text = messageParts.First(part => part.MimeType == "text/plain").Body.Data;
                        break;
                    }
                    else
                    {
                        messageParts = messageParts.First(part => part.Parts != null).Parts;
                    }
                }
            }
            ///google does replace + to - and / to _
            base64Text = base64Text.Replace('-', '+').Replace('_', '/');
            gmailReply.Text = Encoding.UTF8.GetString(Convert.FromBase64String(base64Text));
            return gmailReply;
        }

    }
}
