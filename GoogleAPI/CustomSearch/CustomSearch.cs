﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Class to perform custom search that is set on CSE.GOOGLE.COM site
    /// </summary>
    public class CustomSearch : GoogleClass
    {
        /// <summary>
        /// Constructor with API key
        /// </summary>
        /// <param name="apiKey">API key</param>
        public CustomSearch(string apiKey)
        {
            if (apiKey == null || apiKey == "")
                throw new ArgumentNullException("ApiKey", "Empty ApiKey");
            this.ApiKey = apiKey;
        }
        /// <summary>
        /// Performs search on custom search engine with given Id and query
        /// </summary>
        /// <param name="searchId">Id of custom search engine in form of xxxxxxxxxxxx:yyyyyyyyyyy</param>
        /// <param name="searchQuery">Search query</param>
        /// <param name="count">Number of search results. Defaults to 30</param>
        /// <param name="fromDate">Date to retrieve results from</param>
        /// <returns>Result with Value set to List of CustomSearchReplies if succesful, otherwise
        /// Value=null and Message is set</returns>
        public Result<List<CustomSearchReply>> PerformSearch(string searchId, string searchQuery, int count = 30, DateTime? fromDate = null)
        {
            if (searchId == null || searchId == "")
                return new Result<List<CustomSearchReply>>(null, "Search ID not provided");
            if (searchQuery == null || searchQuery == "")
                return new Result<List<CustomSearchReply>>(null, "Search Query not provided");
            if (count <= 0)
                return new Result<List<CustomSearchReply>>(null, "Count should be more than zero");
            if (fromDate != null && fromDate.Value > DateTime.Now)
                return new Result<List<CustomSearchReply>>(null, "From date cannot be in future");
            try
            {
                var customSearchReplyList = new List<CustomSearchReply>();
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<List<CustomSearchReply>>(null, baseClient.Message);
                var customSearchService = new Google.Apis.Customsearch.v1.CustomsearchService(baseClient.Value);
                var customSearchAction = customSearchService.Cse.List(searchQuery);
                customSearchAction.Cx = searchId;
                if (fromDate != null)
                {
                    customSearchAction.DateRestrict = "d" + Math.Round((DateTime.Now - fromDate.Value).TotalDays);
                }
                var startIndex = 1;
                customSearchAction.Start = startIndex;
                var customSearch = customSearchAction.Execute();
                while (customSearch.Items != null || customSearch.Items.Count != 0)
                {
                    foreach (var item in customSearch.Items)
                    {
                        if (customSearchReplyList.Count >= count)
                            break;
                        var customSearchReply = new CustomSearchReply()
                        {
                            Link = item.Link,
                            Snippet = item.Snippet,
                            Title = item.Title
                        };
                        customSearchReplyList.Add(customSearchReply);
                    }
                    if (customSearchReplyList.Count >= count)
                        break;
                    if (customSearch.Queries.ContainsKey("nextPage"))
                    {
                        customSearchAction.Start = customSearch.Queries["nextPage"][0].StartIndex.Value;
                        customSearch = customSearchAction.Execute();
                    }
                    else
                        break;
                }
                return new Result<List<CustomSearchReply>>(customSearchReplyList);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<List<CustomSearchReply>>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Google.GoogleApiException)
            {
                return new Result<List<CustomSearchReply>>(null, "Wrong search ID was given");
            }
            catch (Exception e)
            {
                return new Result<List<CustomSearchReply>>(null, "Unhandled exception");
            }
        }

    }
}
