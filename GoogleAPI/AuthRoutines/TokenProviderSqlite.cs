﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Data.SQLite;
using System.IO;
using SQLite;

namespace API.GoogleAPI.Auth
{
    /// <summary>
    /// Default token provider implementation using embedded SQLite DB
    /// </summary>
    internal class TokenProviderSqlite : TokenProvider
    {
        /// <summary>
        /// Internal exception class that is thrown when there is already token with given primary key(id)
        /// </summary>
        internal class DuplicateException : Exception { }
        /// <summary>
        /// Database filename
        /// </summary>
        public string DbFilename { get; set; }
        /// <summary>
        /// Creates Token Provider from SQLITE DB class using dbFile given. If file is not present we will create one
        /// </summary>
        /// <param name="dbFile">Full path or relative path to file containing DB</param>
        /// <param name="newDb">If set to true and file doesn't exist new database is created</param>
        public TokenProviderSqlite(string dbFile,bool newDb=false)
        {
            /// <exception cref="ArgumentException">One of the arguments was incorrect</exception>
            DbFilename = dbFile;
            if (dbFile == null || dbFile == "")
                throw new ArgumentException("dbFile", "Empty dbFile");
                if (!File.Exists(dbFile))
                {
                if (newDb)
                    _CreateAndPopulateDb();
                else
                    throw new ArgumentException("dbFile", "No such database file");
                }
            
        }
        /// <summary>
        /// Creates database and also creates table to retrieve/save token data 
        /// </summary>
        /// <param name="dbFile">Database Filename</param>
        private void _CreateAndPopulateDb()
        {
            using (var connection = new SQLiteConnection(DbFilename))
            {
                connection.CreateTable<Token>();
                connection.Close();
            }
        }
        /// <summary>
        /// Gets Token from sqlite database by id
        /// </summary>
        /// <param name="id">ID of the botapi user</param>
        /// <param name="applicationName">Name of the application(scope)</param>
        /// <returns>Result with Result.Value set to token object or Result.Message that explains lack of token</returns>
        public Result<Token> GetTokenForId(int id,string applicationName)
        {
            try
            {
                using (var connection = new SQLiteConnection(DbFilename))
                {
                    if (connection.GetTableInfo("Token").Count == 0)
                        return new Result<Token>(null, "Database doesn't appear to have correct structure");
                    var token = connection.Table<Token>()
                        .FirstOrDefault(tokenRow => tokenRow.Id == id && tokenRow.ApplicationName == applicationName);
                    connection.Close();
                    return new Result<Token>(token);
                }
            }
            catch (Exception e)
            {
                return new Result<Token>(null, "Unhandled exception", e);
            }
        }
        /// <summary>
        /// Saves given Token object to database
        /// </summary>
        /// <param name="id">Id of the botapi. In this particular case this parameter is redundant, as id is inside token object</param>
        /// <param name="token">Token object to save</param>
        /// <returns>Result with value set to true if succesful, otherwise false and message is given</returns>
        public Result<bool> SaveTokenForId(int id, Token token)
        {
            try
            {
                using (var connection = new SQLiteConnection(DbFilename))
                {
                    if (connection.GetTableInfo("Token").Count == 0)
                        return new Result<bool>(false, "Database doesn't appear to have correct structure");
                    if (connection.Table<Token>().Any(tokenFromDb => 
                    tokenFromDb.Id == token.Id 
                    && tokenFromDb.ApplicationName==token.ApplicationName))
                        throw new DuplicateException();
                    ///safely inserting
                    connection.BeginTransaction();
                    connection.Insert(token);
                    connection.Commit();
                    connection.Close();
                    return new Result<bool>(true);
                }
            }
            catch (DuplicateException)
            {
                return new Result<bool>(false, "Token with given ID and app already exists");
            }
            catch (Exception e)
            {
                return new Result<bool>(false, "Unhandled exception", e);
            }
        }
    }
}
