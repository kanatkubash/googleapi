﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Custom Search Engines reply item
    /// </summary>
    public class CustomSearchReply
    {
        /// <summary>
        /// Link to the search result
        /// </summary>
        public string Link { get; set; }
        /// <summary>
        /// Title of the search result
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Snippet(part) of the search result
        /// </summary>
        public string Snippet { get; set; }
    }
}
