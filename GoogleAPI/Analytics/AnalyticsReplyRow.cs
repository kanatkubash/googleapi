﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI.AnalyticsReplyHelper
{
    /// <summary>
    /// Class to provide realtime details on resource
    /// </summary>
    public class AnalyticsReplyRow
    {
        /// <summary>
        /// Country of visitor
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// City of visitor
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Type of visitor : NEW or RETURNING
        /// </summary>
        public string UserType { get; set; }
        /// <summary>
        /// Count of visitors that correspond to given values of Country , City, UserTYpe etc.
        /// </summary>
        public int Count { get; set; }
    }
}
