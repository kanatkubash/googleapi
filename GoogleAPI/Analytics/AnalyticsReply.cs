﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Analytics API uses this class as reply for analytics function calls
    /// </summary>
    public class AnalyticsReply
    {
        /// <summary>
        /// This class is instansiated when Analytics API called to retrieve realtime data
        /// </summary>
        public class RealtimeData
        {
            /// <summary>
            /// Protected so that it cannot be instansiated directly
            /// </summary>
            protected RealtimeData()
            {
                Rows = new List<AnalyticsReplyHelper.AnalyticsReplyRow>();
            }
            /// <summary>
            /// Total amount of realtime users that are on resource 
            /// </summary>
            public int RealtimeTotalUsers { get; set; }
            /// <summary>
            /// List of realtime user analytics by country, by city etc.
            /// </summary>
            public List<API.GoogleAPI.AnalyticsReplyHelper.AnalyticsReplyRow> Rows { get; set; }
        }
        /// <summary>
        /// This class is instansiated when Analytics API called to retrieve non-realtime data for certain period
        /// </summary>
        public class GaData
        {
            /// <summary>
            /// Protected so that it cannot be instansiated directly
            /// </summary>
            protected GaData()
            {
                Rows = new List<AnalyticsReplyHelper.AnalyticsReplyRowDetailed>();
            }
            /// <summary>
            /// Total amount of users that visited the resource for given periods
            /// </summary>
            public int TotalUsers { get; set; }
            /// <summary>
            /// Total count of pages that have been visited by users
            /// </summary>
            public int TotalPageViews { get; set; }
            /// <summary>
            /// List of aggregate data grouped by browser,page path,country etc.
            /// </summary>
            public List<API.GoogleAPI.AnalyticsReplyHelper.AnalyticsReplyRowDetailed> Rows { get; set; }
        }
        /// <summary>
        /// Wrapper is needed to make RealtimeData non-instansiable
        /// </summary>
        private class RealtimeDataWrapper : RealtimeData { }
        /// <summary>
        /// Wrapper is needed to make GaData non-instansiable
        /// </summary>
        private class GaDataWrapper : GaData { }
        /// <summary>
        /// Constructor for Analytics API reply
        /// </summary>
        /// <param name="isRealtime">To distinguish between realtime and data for period of time</param>
        public AnalyticsReply(bool isRealtime)
        {
            if (isRealtime)
            {
                ///If realtime Realtime field is initialized
                Realtime = new RealtimeDataWrapper();
                Ga = null;
            }
            else
            {
                //////Otherise Ga field which is responsible for data for given periods of time is inited
                Ga = new GaDataWrapper();
                Realtime = null;
            }
            IsRealtime = isRealtime;
        }
        /// <summary>
        /// Flag showing whether data is realtime or not
        /// </summary>
        public bool IsRealtime { get; set; }
        /// <summary>
        /// Field for getting realtime data if present
        /// </summary>
        public RealtimeData Realtime;
        /// <summary>
        /// Field for getting nonrealtime data if present
        /// </summary>
        public GaData Ga;
    }
}


