﻿using API.GoogleAPI.NLReplyHelper;
using Google.Apis.CloudNaturalLanguageAPI.v1beta1;
using Google.Apis.CloudNaturalLanguageAPI.v1beta1.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Class that provides Natural Language methods from Google NL API
    /// </summary>
    public class NaturalLanguage : GoogleClass
    {
        /// <summary>
        /// Constructor of Natural Language
        /// </summary>
        /// <param name="apiKey"></param>
        public NaturalLanguage(string apiKey)
        {
            if (apiKey == null || apiKey == "")
                throw new ArgumentNullException("ApiKey", "Empty ApiKey");
            this.ApiKey = apiKey;
        }
        /// <summary>
        /// Get Entities such as Location, Person from given text
        /// </summary>
        /// <param name="text">Text to perform entity search</param>
        /// <returns>if succesful NLReply with EntityData set is returned, otherwise Result.Value=null and message is given</returns>
        public Result<NLReply> GetEntities(string text)
        {
            if (text == null || text == "")
            {
                return new Result<NLReply>(null, "Text not provided");
            }
            try
            {
                ///List of entities that Google NL API found from text
                var entityList = new List<NLEntity>();
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<NLReply>(null, baseClient.Message);
                var nlService = new CloudNaturalLanguageAPIService(baseClient.Value);
                var entityRequest = new AnalyzeEntitiesRequest();
                ///Input text is given as Document object
                entityRequest.Document = new Document() { Content = text, Type = "PLAIN_TEXT" };
                var analyzeAction = nlService.Documents.AnalyzeEntities(entityRequest);
                var analyze = analyzeAction.Execute();
                foreach (var entity in analyze.Entities)
                {
                    ///Parse each reply to our format
                    entityList.Add(_ParseEntity(entity));
                }
                var nlReply = new NLReply() { EntityData = entityList };
                return new Result<NLReply>(nlReply);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<NLReply>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<NLReply>(null, "Unhandled exception", e);
            }
        }
        /// <summary>
        /// Parses each Google.Apis.CloudNaturalLanguageAPI.v1beta1.Data.Entity object to our NLEntity format
        /// </summary>
        /// <param name="entity">Google.Apis.CloudNaturalLanguageAPI.v1beta1.Data.Entity object</param>
        /// <returns>NLEntity object</returns>
        private NLEntity _ParseEntity(Entity entity)
        {
            var nlEntity = new NLEntity()
            {
                Name = entity.Name,
                ///Each mention will have the same text
                Text = entity.Mentions.First().Text.Content,
                Type = entity.Type,
                Salience = entity.Salience
            };
            ///Currently only wikipedia url is provided if present
            if (entity.Metadata != null && entity.Metadata.ContainsKey("wikipedia_url"))
                nlEntity.RelatedUri = entity.Metadata["wikipedia_url"];
            return nlEntity;
        }
        /// <summary>
        /// Get sentiment of given text
        /// </summary>
        /// <param name="text">Text to analyze</param>
        /// <returns>If succesful, Result with NLReply where only SentimentData is set, otherwise
        /// Result.Value=null and message is given</returns>
        public Result<NLReply> GetSentiment(string text)
        {
            if (text == null || text == "")
            {
                return new Result<NLReply>(null, "Text not provided");
            }
            try
            {
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<NLReply>(null, baseClient.Message);
                var nlService = new CloudNaturalLanguageAPIService(baseClient.Value);
                var sentimentRequest = new AnalyzeSentimentRequest();
                sentimentRequest.Document = new Document() { Content = text, Type = "PLAIN_TEXT" };
                var analyzeAction = nlService.Documents.AnalyzeSentiment(sentimentRequest);
                var sentiment = analyzeAction.Execute();
                var nlReply = new NLReply()
                {
                    SentimentData = new NLSentiment()
                    {
                        Magnitude = sentiment.DocumentSentiment.Magnitude,
                        Polarity = sentiment.DocumentSentiment.Polarity
                    }
                };
                return new Result<NLReply>(nlReply);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<NLReply>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Exception e)
            {
                return new Result<NLReply>(null, "Unhandled exception", e);
            }
        }
    }
}

