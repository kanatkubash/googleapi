﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Translate.v2;
namespace API.GoogleAPI
{
    public class Translate : GoogleClass
    {
        /// <summary>
        /// Class to perform translation of text
        /// </summary>
        /// <param name="apiKey">ApiKey</param>
        public Translate(string apiKey)
        {
            if (apiKey == null || apiKey == "")
                throw new ArgumentNullException("ApiKey", "Empty ApiKey");
            this.ApiKey = apiKey;
        }
        /// <summary>
        /// Translates the text to target language.
        /// </summary>
        /// <param name="text">Text to translate</param>
        /// <param name="targetLanguage">Target language in ISO 639-1 format</param>
        /// <param name="sourceLanguage">Source language in ISO 639-1 format. If not given, auto detected</param>
        /// <returns>Result with Translation of text if succesful, otherwise Result.Value=null and message is given</returns>
        public Result<TranslateReply> TranslateText(string text, string targetLanguage, string sourceLanguage = null)
        {
            if (text == null || text == "")
                return new Result<TranslateReply>(null, "Text not provied");
            if (targetLanguage == null || targetLanguage == "")
                return new Result<TranslateReply>(null, "Target language not provided");
            try
            {
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<TranslateReply>(null, baseClient.Message);
                var translateService = new TranslateService(baseClient.Value);
                ///TranslateService translates only arrays, so transform text string to array of strings
                var words = new Google.Apis.Util.Repeatable<string>(new string[] { text });
                var translateAction = translateService.Translations.List(words, targetLanguage);
                if (sourceLanguage != null && sourceLanguage.Trim() != "")
                    translateAction.Source = sourceLanguage.Trim();
                var translations = translateAction.Execute();
                var translationReply = new TranslateReply();
                translationReply.Translation = translations.Translations.First().TranslatedText;
                translationReply.SourceLanguage = translations.Translations.First().DetectedSourceLanguage;
                return new Result<TranslateReply>(translationReply);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<TranslateReply>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (Google.GoogleApiException)
            {
                return new Result<TranslateReply>(null, "Incorrect target language was given");
            }
            catch (Exception e)
            {
                return new Result<TranslateReply>(null, "Unhandled exception", e);
            }
        }
    }
}
