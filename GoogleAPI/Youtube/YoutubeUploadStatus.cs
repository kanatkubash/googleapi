﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Class that is sent to user callback function  during youtube video upload
    /// </summary>
    public class YoutubeUploadStatus
    {
        /// <summary>
        /// Exception that occured during upload
        /// </summary>
        public Exception Exception { get; set; }
        /// <summary>
        /// Status enumeration
        /// </summary>
        public enum Statuses { Uploading, Finished, Failed };
        /// <summary>
        /// Percent of overall progress. Calculated by dividing sent bytes to filesize
        /// </summary>
        public long Progress { get; set; }
        /// <summary>
        /// Current status of upload
        /// </summary>
        public Statuses Status { get; set; }
    }
}
