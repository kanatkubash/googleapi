﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI.AnalyticsReplyHelper
{
    /// <summary>
    /// Class to provide non realtime details on resource
    /// </summary>
    public class AnalyticsReplyRowDetailed
    {
        /// <summary>
        /// Country of visitor
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// City of visitor
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Type of visitor : NEW or RETURNING
        /// </summary>
        public string UserType { get; set; }
        /// <summary>
        /// Date when this analytical data is gathered
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Path of webpage visitors have viewed 
        /// </summary>
        public string PagePath { get; set; }
        /// <summary>
        /// Browser that visitor used to view the webpage
        /// </summary>
        public string Browser { get; set; }
        /// <summary>
        /// Amount of visitors corresponding to given fields (Country,PageView,Date etc.)
        /// </summary>
        public int Users { get; set; }
        /// <summary>
        /// Amount of pageviews performed by visitors
        /// </summary>
        public int PageViews { get; set; }
    }
}
