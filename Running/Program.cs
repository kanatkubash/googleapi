﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Diagnostics;
using System.Threading;
using API.GoogleAPI;

namespace Running
{
    class Program
    {
        static string ApiKey { get; set; }
        static string ClientId { get; set; }
        static string ClientSecret { get; set; }
        static void Main(string[] args)
        {
            //API key
            ApiKey = "AIzaSyAv_KR4Uey1Q3r5ZR5Md5lSRUZF-uVZIHo";
            //OAuth ClientID
            ClientId = "666545560121-t1da4icri87r5ugimq5s0fu6bvc0d3ee.apps.googleusercontent.com";
            //OAuth ClientSecret
            ClientSecret = "YasHUp_nktahPvkoWBDqX8lK";
            ///public domain name (host) where this code is running. 
            ///This is needed to handle redirect urls as Google requires domain names instead of just IP
            ///For example purposes we set it to localhost
            API.GoogleAPI.GoogleClass.DomainName = "localhost";
            ///Id if the bot. Each bot needs to be identified uniquely to get exact tokens.
            int idOfTheBot = 11032;
            string json = "";
            #region ANALYTICS
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = AnalyticsGetDataForPeriod(idOfTheBot);
            Console.WriteLine(json);
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = AnalyticsGetRealtimeData(idOfTheBot);
            Console.WriteLine(json);
            #endregion
            #region CALENDAR
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = CalendarCheckAvailability(idOfTheBot);
            Console.WriteLine(json);
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = CalendarSetMeeting(idOfTheBot);
            Console.WriteLine(json);
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = CalendarMoveMeeting(idOfTheBot);
            Console.WriteLine(json);
            #endregion
            #region CUSTOM_SEARCH
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = CustomSearchPerformSearch();
            Console.WriteLine(json);
            #endregion
            #region GMAIL
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = GmailGetUnreadMessages(idOfTheBot);
            Console.WriteLine(json);
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = GmailSendMessage(idOfTheBot);
            Console.WriteLine(json);
            #endregion
            #region NATURAL_LANG
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = NaturalLanguageGetEntities();
            Console.WriteLine(json);
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = NaturalLanguageGetSentiment();
            Console.WriteLine(json);
            #endregion
            #region TRANSLATE
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = TranslateTranslateText();
            Console.WriteLine(json);
            #endregion
            #region YOUTUBE
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = YoutubeGetVideoUpdateFromDate(idOfTheBot);
            Console.WriteLine(json);
            Console.WriteLine("\r\nPress enter to proceed.\r\n");
            Console.ReadLine();
            json = YoutubePostVideo(idOfTheBot);
            Console.WriteLine(json);
            #endregion
        }
        static string AnalyticsGetDataForPeriod(int idOfTheBot)
        {
            var analyticsAPI = new API.GoogleAPI.Analytics(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = analyticsAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Otherwise proceed
                ///
                ///Analytics account name
                var accountName = "blabla@gmail.com";
                ///Name of the view
                var viewName = "All web site data";
                ///Period of time to gather data from. In our example its week ago
                var fromDate = DateTime.Now - TimeSpan.FromDays(7);
                ///End date of analytics data. In our example its till today
                var toDate = DateTime.Now;
                ///If there are many views select which exact website(resource) we want to view
                //var site= "example.com";

                var result = analyticsAPI.GetDataForPeriod(accountName, viewName, fromDate, toDate);
                return new JsonResult(result.Value, result.Message);
            }
        }
        static string AnalyticsGetRealtimeData(int idOfTheBot)
        {
            var analyticsAPI = new API.GoogleAPI.Analytics(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = analyticsAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {

                ///Analytics account name
                var accountName = "blabla@gmail.com";
                ///Name of the view
                var viewName = "All website data";
                ///If there are many views select which exact website(resource) we want to view
                var site = "example.com";
                var result = analyticsAPI.GetRealtimeData(accountName, viewName, site);
                return new JsonResult(result.Value, result.Message);
            }
        }
        static string CalendarCheckAvailability(int idOfTheBot)
        {
            var calendarAPI = new API.GoogleAPI.Calendar(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = calendarAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Name of the calendar. 
                var calendarName = "blabla@gmail.com";
                ///start date 
                var fromDate = Convert.ToDateTime("2016-08-28 09:00:00");
                ///End date
                var toDate = Convert.ToDateTime("2016-08-28 12:00:00");
                var result = calendarAPI.CheckAvailability(calendarName, fromDate, toDate);
                return new API.GoogleAPI.JsonResult(result.Value, result.Message);
            }
        }
        static string CalendarSetMeeting(int idOfTheBot)
        {
            var calendarAPI = new API.GoogleAPI.Calendar(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = calendarAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Name of the calendar. 
                var calendarName = "blabla@gmail.com";
                ///Name of the meeting
                var meetingName = "Meeting with SEO and PR team";
                ///start date 
                var fromDate = Convert.ToDateTime("2016-08-28 14:00:00");
                ///End date. Two hours
                var toDate = Convert.ToDateTime("2016-08-28 16:00:00");
                var result = calendarAPI.SetMeeting(calendarName, meetingName, fromDate, toDate);
                return new JsonResult(result.Value, result.Message);
            }
        }
        static string CalendarMoveMeeting(int idOfTheBot)
        {
            var calendarAPI = new API.GoogleAPI.Calendar(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = calendarAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Name of the calendar. 
                var calendarName = "blabla@gmail.com";
                ///Name of the meeting
                var meetingName = "Meeting with guys from ABA Group";
                ///new meeting start date 
                var fromDate = Convert.ToDateTime("2016-08-30 09:00:00");
                ///new meeting end date
                var toDate = Convert.ToDateTime("2016-08-30 12:00:00");
                var result = calendarAPI.MoveMeeting(calendarName, meetingName, fromDate, toDate);
                return new JsonResult(result.Value, result.Message);
            }
        }
        static string CustomSearchPerformSearch()
        {
            var customSearchApi = new API.GoogleAPI.CustomSearch(Program.ApiKey);
            ///Id of the customsearch . 
            var customSearchId = "02390394039:blabla";
            ///Search query
            var searchQuery = "search term";
            ///Filter search results by start date
            var weekendAgo = DateTime.Now - TimeSpan.FromDays(7);
            var result = customSearchApi.PerformSearch(customSearchId, searchQuery, 100, weekendAgo);
            return new JsonResult(result.Value, result.Message);
        }
        static string GmailGetUnreadMessages(int idOfTheBot)
        {
            var gmailAPI = new API.GoogleAPI.Gmail(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = gmailAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Gmail user name
                var userName = "blabla@gmail.com";
                var dayAgo = DateTime.Now - TimeSpan.FromDays(1);
                var result = gmailAPI.GetUnreadMessages(userName, dayAgo);
                return new JsonResult(result.Value, result.Message);
            }
        }
        static string GmailSendMessage(int idOfTheBot)
        {
            var gmailAPI = new API.GoogleAPI.Gmail(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = gmailAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Gmail user name
                var recipient = "blabla@gmail.com";
                ///Message subject
                var subject = "Product details";
                ///Message body
                var text = "Hello Mr. Could you provide details for product 0029422-44?";
                var result = gmailAPI.SendMessage(recipient, subject, text);
                return new JsonResult(result.Value, result.Message);
            }
        }
        static string NaturalLanguageGetEntities()
        {
            var nlAPI = new API.GoogleAPI.NaturalLanguage(Program.ApiKey);
            ///text to analyze for entities
            var text = "The wife of Prime Minister Shinzo Abe has visited Pearl Harbor, bombed by "
                + "Japanese planes nearly 75 years ago in an attack that brought the United States into World War II, AFP reports.";
            var result = nlAPI.GetEntities(text);
            return new JsonResult(result.Value, result.Message);
        }
        static string NaturalLanguageGetSentiment()
        {
            var nlAPI = new API.GoogleAPI.NaturalLanguage(Program.ApiKey);
            ///text to analyze for entities
            var text = "The wife of Prime Minister Shinzo Abe has visited Pearl Harbor, bombed by "
                + "Japanese planes nearly 75 years ago in an attack that brought the United States into World War II, AFP reports.";
            var result = nlAPI.GetSentiment(text);
            return new JsonResult(result.Value, result.Message);
        }
        static string TranslateTranslateText()
        {
            var translateAPI = new API.GoogleAPI.Translate(Program.ApiKey);
            ///text to translate
            var text = "The wife of Prime Minister Shinzo Abe has visited Pearl Harbor, bombed by "
                + "Japanese planes nearly 75 years ago in an attack that brought the United States into World War II, AFP reports.";
            ///target language
            var russianLanguage = "ru";
            var result = translateAPI.TranslateText(text, russianLanguage);
            return new JsonResult(result.Value, result.Message);
        }
        static string YoutubePostVideo(int idOfTheBot)
        {
            var youtubeAPI = new API.GoogleAPI.Youtube(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = youtubeAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Full path to file
                var fileName = "C:\\videotopost.mp4";
                ///Title of video to post
                var title = "Interesting video";
                ///Description
                var description = "Description of interesting video that I'm posting";
                ///Channel to post video
                var channel = "blabla";
                var callbackFunc = new Action<API.GoogleAPI.YoutubeUploadStatus>((inParam) =>
                {
                    var uploadStatus = (API.GoogleAPI.YoutubeUploadStatus)inParam;
                    Console.WriteLine("Upload percentage: " + uploadStatus.Progress);
                    if (uploadStatus.Status == API.GoogleAPI.YoutubeUploadStatus.Statuses.Failed)
                        Console.WriteLine("Failed uploading");
                });
                var result = youtubeAPI.PostVideo(fileName, title, description, channel, callbackFunc);
                return new JsonResult(result.Value, result.Message);
            }
        }
        static string YoutubeGetVideoUpdateFromDate(int idOfTheBot)
        {
            var youtubeAPI = new API.GoogleAPI.Youtube(Program.ClientId, Program.ClientSecret);
            ///Authorize/authenticate given id before API use. 
            ///Reply.Value can be bool meaning its authorized
            ///or
            ///Reply.Value can be string meaning its authorization URI
            var reply = youtubeAPI.AuthorizeForId(idOfTheBot);
            if (reply.Value is string)
            {
                ///We have been sent authorize URI.
                ///Process it
                var process = new Process() { StartInfo = new ProcessStartInfo(reply.Value.ToString()) }.Start(); ;
                return new API.GoogleAPI.JsonResult(null, "You haven't authorized. Authorize using this URI " + reply.Value);
            }
            else
            {
                ///Name of the channel
                var channelName = "WatchMojo";
                ///Get week ago updates
                var weekAgo = DateTime.Now - TimeSpan.FromDays(7);
                var result = youtubeAPI.GetVideoUpdateFromDate(channelName, weekAgo);
                return new JsonResult(result.Value, result.Message);
            }
        }
    }
}
