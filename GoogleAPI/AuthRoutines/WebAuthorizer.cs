﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace API.GoogleAPI.Auth
{
    /// <summary>
    /// Class responsible for generating redirect_uri for Oauth token, 
    /// handling Oauth redirect callbacks
    /// and providing Oauth token itself during the process
    /// </summary>
    internal sealed class WebAuthorizer
    {
        /// <summary>
        /// Range end of ports to listen for redirect Url
        /// </summary>
        private const int PORT_END = 13000;
        /// <summary>
        /// Range start of ports to listen for redirect Url
        /// </summary>
        private const int PORT_START = 12000;
        /// <summary>
        /// Google Oauth initial url for getting access code for later token retrieval
        /// </summary>
        private const string OAUTH_URL = "https://accounts.google.com/o/oauth2/auth?";
        /// <summary>
        /// Google Oauth url for exchanging access code to token
        /// </summary>
        private const string OAUTH_TOKEN_URL = "https://accounts.google.com/o/oauth2/token";
        /// <summary>
        /// Regex to retrieve first line of HTTP request containing code parameter in GET request
        /// code itself contained in (.*) capture group
        /// </summary>
        private const string CODE_RETRIEVAL_PATTERN = @"^GET \/\?code=(.*) HTTP\/\d\.\d";
        /// <summary>
        /// IPv4 address to bind redirect_url listener
        /// </summary>
        private IPAddress Address { get; set; }
        /// <summary>
        /// Domain name to include in redirect uri, as we cant put there ip address
        /// </summary>
        private string DomainName { get; set; }
        /// <summary>
        /// List of used ports by listener
        /// </summary>
        private List<int> UsedPorts { get; set; }
        /// <summary>
        /// Creates new WebAuthorizer class. Constructor made private to prevent direct instansing
        /// as by architecture there must be only one WebAuthorizer
        /// </summary>
        /// <param name="domainName">Domain Name used in Redirect Uri generation</param>
        /// <returns>Instance of WebAuthorizer class</returns>
        public static WebAuthorizer Create(string domainName)
        {
            var authorizer = new WebAuthorizer(domainName);
            return authorizer;
        }
        /// <summary>
        /// WebAuthorizer constructor. Cannot be called directly to ensure that there is only one listener
        /// </summary>
        /// <param name="domainName"></param>
        private WebAuthorizer(string domainName)
        {
            Address = _ParseIpAddress(domainName);
            DomainName = domainName;
            UsedPorts = new List<int>();
        }
        /// <summary>
        /// Gets url for authorization to send it to user
        /// </summary>
        /// <param name="callback">Callback to signal </param>
        /// <returns></returns>
        public Result<string> GetUrlForAuthorization(Google.Apis.Auth.OAuth2.ClientSecrets secret, IEnumerable<string> scopes, Action<Token> callback)
        {
            try
            {
                ///Creating listener that listens for Google Oauth callback
                var listener = _CreateListener();
                var redirectUrl = String.Format("http://{0}:{1}/", DomainName, ((IPEndPoint)listener.LocalEndpoint).Port);
                var url = _CreateUrl(redirectUrl, secret.ClientId, scopes);
                listener.Start();
                var paramsToPass = new object[]
                {
                    listener,
                    callback,
                    secret,
                    redirectUrl
                };
                listener.BeginAcceptTcpClient(_HandleIncomingConnection, paramsToPass);
                return new Result<string>(url);
            }
            catch (Exception e)
            {
                return new Result<string>(null, "Unhandled exception", e);
            }

        }
        /// <summary>
        /// Returns GET url from given parameters
        /// </summary>
        /// <param name="redirectUrl">Url to redirect after succesful login</param>
        /// <param name="clientId">ClientId of an API</param>
        /// <param name="scopes">Scopes of application</param>
        /// <returns>Authorization uri</returns>
        private string _CreateUrl(string redirectUrl, string clientId, IEnumerable<string> scopes)
        {
            var parameters = new Dictionary<string, string>()
            {
                { "client_id",  clientId},
                { "redirect_uri", redirectUrl},
                ///list of scopes as one line string delimited by space
                {"scope", scopes.Aggregate((a,b)=>a+" "+b ) },
                {"response_type", "code" },
                {"access_type","offline" },
                { "approval_prompt","force" }
            };
            ///select (a=b) then concat (a=b&c=d)
            var parameterString = parameters
                        .Select(pair => pair.Key + "=" + Uri.EscapeDataString(pair.Value))
                        .Aggregate((a, b) => a + "&" + b);
            return OAUTH_URL + parameterString;
        }
        /// <summary>
        /// Randomly chooses available port and returns listener for this port
        /// </summary>
        /// <returns>Listener object that is bound to unused random chosen port</returns>
        private TcpListener _CreateListener()
        {
            int port = 0;
            TcpConnectionInformation[] tcpConnections = IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpConnections();
            ///choose random port and check whether its available
            do
            {
                port = new Random().Next(PORT_START, PORT_END);
            }
            while (UsedPorts.Contains(port) && tcpConnections.Any(conn => conn.LocalEndPoint.Port == port));
            UsedPorts.Add(port);
            var tcpListener = new TcpListener(Address, port);
            return tcpListener;
        }
        /// <summary>
        /// Checks given domain and retrieves IPv4 address from it. If domain not resolved or no suitable ipv4 found throws Exception
        /// </summary>
        /// <param name="domain"></param>
        /// <exception cref="Exception">Throws Exception if no ipv4 found</exception>
        /// <exception cref="SocketException">Thrown when host not resolved</exception>
        private IPAddress _ParseIpAddress(string domain)
        {
            var addresses = Dns.GetHostAddresses(domain);
            if (!addresses.Any(address => address.AddressFamily == AddressFamily.InterNetwork))
            {
                throw new Exception("No suitable IPV4 address found");
            }
            return addresses.First(address => address.AddressFamily == AddressFamily.InterNetwork);
        }
        /// <summary>
        /// Handles incoming HTTP connection. If accessCode is succesfully found, 
        /// _RetrieveTokensFromAccessCode is used to get tokens using this accessCode
        /// </summary>
        /// <param name="res"></param>
        private void _HandleIncomingConnection(IAsyncResult res)
        {
            try
            {
                ///Object type passed as res.AsyncState parameter
                ///is array consisting of following objects
                ///    TcpListener listener,
                ///    Action<Token> callback,
                ///    Google.Apis.Auth.OAuth2.ClientSecrets secret,
                ///   String redirect_url
                ///Passing in form of object array is due to fact that only one parameter can be passed as res.AsyncState;
                var listener = (res.AsyncState as object[])[0] as TcpListener;
                var callbackAction = (res.AsyncState as object[])[1] as Action<Token>;
                var secret = (res.AsyncState as object[])[2] as Google.Apis.Auth.OAuth2.ClientSecrets;
                var redirect_uri = (res.AsyncState as object[])[3] as string;
                string httpRawString = "";
                ///cascade of usings to properly dispose resources
                using (var client = listener.EndAcceptTcpClient(res))
                {
                    using (var stream = client.GetStream())
                    {
                        var bytes = new byte[1024];
                        ///to prevent excessive method block
                        stream.ReadTimeout = 2000;
                        stream.WriteTimeout = 2000;
                        int byteCount = stream.Read(bytes, 0, bytes.Length);
                        httpRawString = System.Text.Encoding.ASCII.GetString(bytes, 0, byteCount);
                        ///say the user that his request is processing
                        _InformOfProcessing(stream);
                    }
                    ///stopping properly everything and releasing port
                    listener.Stop();
                    if (listener.Server != null)
                        listener.Server.Close();
                    UsedPorts.Remove(((IPEndPoint)listener.LocalEndpoint).Port);
                }
                if (httpRawString == null || httpRawString == "")
                    return;
                var matches = Regex.Match(httpRawString, CODE_RETRIEVAL_PATTERN);
                if (!matches.Success) return;
                var accessCode = matches.Groups[matches.Groups.Count - 1].Value;
                var token = _RetrieveTokensFromAccessCode(accessCode, secret, redirect_uri);
                if (token != null)
                    callbackAction(token);
            }
            catch (Exception e)
            {
                ///This is unusual use of Result class to log exception
                new Result<object>(null, null, e);
            }
        }
        /// <summary>
        /// Informing user on browser that his request is being processed. Just to inform
        /// </summary>
        /// <param name="stream">Underlying network stream to write HTTP response</param>
        private void _InformOfProcessing(NetworkStream stream)
        {
            try
            {
                ///using only ASCII in HTTP body , since ASCII 1 char  = 1 byte. 
                ///This is to set Content-Length as string length rather than byte length
                var httpBody =
                    @"<html>
                    <head>
                        <title>
                            Processing
                        </title>
                    </head>
                    <body>
                        <script>
                        //autoclose
                        window.onload=setTimeout(function(){window.close()},500);
                        </script>
                        <div style='font-familty:""Roboto"",""Arial""'>
                            Request is being processed...You can close the window
                        </div>
                    </body>
                </html>";
                var httpHeaders = new string[]
                {
                "HTTP/1.1 200 OK",
                "Server: Google Auth Perform Server is responding",
                String.Format("Content-Length: {0}", httpBody.Length ),
                "Content-Type: text/html",
                "Connection: Closed"
                };
                var httpResponseString = httpHeaders.Aggregate((a, b) => a + "\r\n" + b)
                    + "\r\n\r\n"
                    + httpBody;
                var httpResponseBytes = System.Text.Encoding.ASCII.GetBytes(httpResponseString);
                stream.Write(httpResponseBytes, 0, httpResponseBytes.Length);
            }
            catch (Exception)
            {
                ///As it's non necessary to do something when particularly 
                ///socket writing fails because its just for informing user purposes
                ///That's why Exception is not recorded
            }
        }
        /// <summary>
        /// In this method WebClient is used to perform further request to Google OAuth server to get Token object
        /// using accessCode
        /// </summary>
        /// <param name="accessCode">Access Code from initial Google OAuth server response</param>
        /// <param name="redirectUri">Redirect Uri that is called by Google OAuth to give token</param>
        /// <param name="secret">API id and secret</param>
        /// <returns>Token object that has access token, refresh token</returns>
        private Token _RetrieveTokensFromAccessCode(string accessCode, Google.Apis.Auth.OAuth2.ClientSecrets secret, string redirectUri)
        {
            try
            {
                var webClient = new WebClient();
                ///Setting contenttype to form data and sending POST 
                ///request to OAuth server to exchange accessCode to Token
                webClient.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";
                var postParams = new Dictionary<string, string>
            {
                {"code",accessCode },
                {"client_id", secret.ClientId},
                {"client_secret",secret.ClientSecret },
                {"redirect_uri",redirectUri },
                {"grant_type","authorization_code" }
            };
                ///select (a=b) then concat (a=b&c=d)
                var postString = postParams
                            .Select(pair => pair.Key + "=" + pair.Value)
                            .Aggregate((a, b) => a + "&" + b);
                var postBytes = System.Text.Encoding.ASCII.GetBytes(postString);
                var responseByte = webClient.UploadData(OAUTH_TOKEN_URL, postBytes);
                var responseJson = System.Text.Encoding.ASCII.GetString(responseByte);
                dynamic obj = JsonConvert.DeserializeObject(responseJson);
                var token = new Auth.Token()
                {
                    AccessToken = obj.access_token,
                    ExpireDate = DateTime.Now + TimeSpan.FromSeconds((double)obj.expires_in),
                    IssueDate = DateTime.Now,
                    RefreshToken = obj.refresh_token,
                    ExpireTime = obj.expires_in
                };
                return token;
            }
            catch (WebException)
            {
                ///Just no internet
                return null;
            }
            catch (Exception e)
            {
                ///This is unusual use of Result class to log exception
                new Result<object>(null, null, e);
                return null;
            }
        }
    }
}
