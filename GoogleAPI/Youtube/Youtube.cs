﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.YouTube.v3;
using Google.Apis.Services;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Auth.OAuth2.Requests;
using Google.Apis.Auth.OAuth2.Responses;
using Google.Apis.YouTube.v3.Data;
using System.IO;
using Google.Apis.Upload;

namespace API.GoogleAPI
{
    /// <summary>
    /// Class to post video to Youtube or get new videos from date from specified channel
    /// </summary>
    public class Youtube : GoogleClass
    {
        /// <summary>
        /// Inner exception class to represent invalid channel name
        /// </summary>
        public class InvalidUsernameException : Exception
        {
        }
        /// <summary>
        /// Youtube API constructor used to only get videos from channel. Doesn't need OAUTH authorization
        /// </summary>
        /// <param name="apiKey">API key</param>
        public Youtube(string apiKey)
        {
            if (apiKey == null || apiKey == "")
                throw new ArgumentNullException("ApiKey", "Empty ApiKey");
            this.Scopes = new[] { YouTubeService.Scope.YoutubeReadonly };
            this.ApiKey = apiKey;
        }
        /// <summary>
        /// Youtube API constructor. Can be used to post or get videos from channel
        /// </summary>
        /// <param name="clientId">Client Id of an API</param>
        /// <param name="clientSecret">Client Secret of an API</param>
        public Youtube(string clientId, string clientSecret)
        {
            if (clientId == null || clientId == "")
                throw new ArgumentNullException("ClientId", "Empty ClientId");
            if (clientSecret == null || clientSecret == "")
                throw new ArgumentNullException("ClientSecret", "Empty ClientSecret");
            this.Scopes = new[] { YouTubeService.Scope.YoutubeReadonly, YouTubeService.Scope.Youtube, YouTubeService.Scope.YoutubeUpload };
            this.Secret = new ClientSecrets()
            {
                ClientId = clientId,
                ClientSecret = clientSecret
            };
            this.ApplicationName = "Youtube";
        }
        /// <summary>
        /// Get videos from channel starting from fromDate
        /// </summary>
        /// <param name="username">Name of the channel. Most channels have same name for user created and channel they own</param>
        /// <param name="fromDate">Start date to filter videos</param>
        /// <returns>Result with Value set to list of YoutubeReply objects if succesful, otherwise
        /// Result.Value=null and message is given</returns>
        public Result<List<YoutubeReply>> GetVideoUpdateFromDate(string username, DateTime fromDate)
        {
            if (username == null || username == "")
                return new Result<List<YoutubeReply>>(null, "Username is empty");
            if (fromDate == null)
                return new Result<List<YoutubeReply>>(null, "Null date provided");
            ///this method can be called without Oauth , but if its called with Oauth then check if authorized
            if (this.Secret!=null && !IsAuthorized)
                    return new Result<List<YoutubeReply>>(null, "Not authorized. Need to authorize first");
            try
            {
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<List<YoutubeReply>>(null, baseClient.Message);
                var youtubeService = new YouTubeService(baseClient.Value);
                ///Get google aware id of channel
                var channelId = _GetChannel(youtubeService, username).Id;
                var listVideoAction = youtubeService.Search.List("snippet");
                listVideoAction.PublishedAfter = fromDate;
                listVideoAction.ChannelId = channelId;
                listVideoAction.MaxResults = 50;
                listVideoAction.Order = SearchResource.ListRequest.OrderEnum.Date;
                listVideoAction.Type = "video";
                SearchListResponse searchResponse = null;
                var youtubeReplyList = new List<YoutubeReply>();
                ///If there are more than 50 videos, do loop while videos end or necessary count of videos reached
                do
                {
                    searchResponse = listVideoAction.Execute();
                    listVideoAction.PageToken = searchResponse.NextPageToken;
                    foreach (var item in searchResponse.Items)
                    {
                        youtubeReplyList.Add(new YoutubeReply()
                        {
                            Id = item.Id.VideoId,
                            Title = item.Snippet.Title,
                            PublishDate = item.Snippet.PublishedAt.GetValueOrDefault()
                        });
                    }
                }
                while (listVideoAction.PageToken != null);
                return new Result<List<YoutubeReply>>(youtubeReplyList);
            }
            catch (System.Net.Http.HttpRequestException httpReqException)
            {
                return new Result<List<YoutubeReply>>(null, "Internet connection problems:" + httpReqException.Message, httpReqException.InnerException);
            }
            catch (InvalidUsernameException)
            {
                return new Result<List<YoutubeReply>>(null, "No such a channel or user");
            }
            catch (Exception e)
            {
                return new Result<List<YoutubeReply>>(null, "Unhandled exception", e);
            }
        }
        /// <summary>
        /// Method providing posting of video given by filename to channel specified by username. Also supports upload progress callback
        /// </summary>
        /// <param name="filename">Full path of the file</param>
        /// <param name="title">Title of the video</param>
        /// <param name="description">Descriptio of the video</param>
        /// <param name="username">Name of the channel</param>
        /// <param name="progressCallback">Callback function to call when progress occurs</param>
        /// <returns>If succesful returns Result.Value=true, otherwise Result.Value=false and message is given</returns>
        public Result<bool> PostVideo(string filename, string title, string description, string username, Action<YoutubeUploadStatus> progressCallback = null)
        {
            if (!File.Exists(filename))
                return new Result<bool>(false, "File doesn't exist");
            if (title == null || title == "")
                return new Result<bool>(false, "Title not provided");
            if (description == null || description == "")
                return new Result<bool>(false, "Description not provided");
            if (username == null || username == "")
                return new Result<bool>(false, "Username not provided");
            if (!Scopes.Contains(YouTubeService.Scope.YoutubeUpload))
                return new Result<bool>(false, "Operation not supported");
            if (!IsAuthorized)
                return new Result<bool>(false, "Not authorized. Need to authorize first");
            try
            {
                var baseClient = GetBaseServiceInitializer();
                if (baseClient.Value == null)
                    return new Result<bool>(false, baseClient.Message);
                var youtubeService = new YouTubeService(baseClient.Value);
                var video = new Video() { Snippet = new VideoSnippet(), Status = new VideoStatus() };
                video.Snippet.Title = title;
                video.Snippet.Description = description;
                video.Status.PrivacyStatus = "public";
                long filesize = new FileInfo(filename).Length;
                using (Stream fileStream = File.Open(filename, FileMode.Open))
                {
                    var insertAction = youtubeService.Videos.Insert(video, "snippet,status", fileStream, "video/*");
                    ///handling progress updates
                    insertAction.ProgressChanged += (update) => _HandleProgress(update, filesize, progressCallback);
                    var uploadProgress = insertAction.Upload();
                    if (uploadProgress.Status == UploadStatus.Completed)
                    {
                        return new Result<bool>(true);
                    }
                    else
                    {
                        return new Result<bool>(false, "Upload failed. Error is:" + uploadProgress.Exception.Message, uploadProgress.Exception.InnerException);
                    }
                }
            }
            catch (InvalidUsernameException)
            {
                return new Result<bool>(false, "No such a channel or user");
            }
            catch (Exception e)
            {
                return new Result<bool>(false, "Unhandled exception occured", e);
            }
        }
        /// <summary>
        /// Inner handling of the youtube update progress
        /// </summary>
        /// <param name="update">Upload progress that youtube service sends</param>
        /// <param name="filesize">Overall filesize of sent video</param>
        /// <param name="progressCallback">User callback function to post video upload progress</param>
        private void _HandleProgress(IUploadProgress update, long filesize, Action<YoutubeUploadStatus> progressCallback)
        {
            YoutubeUploadStatus.Statuses status = YoutubeUploadStatus.Statuses.Uploading;
            switch (update.Status)
            {
                case UploadStatus.Completed: status = YoutubeUploadStatus.Statuses.Finished; break;
                case UploadStatus.Failed: status = YoutubeUploadStatus.Statuses.Failed; break;
                case UploadStatus.NotStarted: status = YoutubeUploadStatus.Statuses.Uploading; break;
                case UploadStatus.Starting: status = YoutubeUploadStatus.Statuses.Uploading; break;
                case UploadStatus.Uploading: status = YoutubeUploadStatus.Statuses.Uploading; break;
            }
            ///If user given a callback function, it's called
            if (progressCallback != null)
                progressCallback(new YoutubeUploadStatus()
                {
                    Progress = (update.BytesSent * 100) / filesize,
                    Status = status
                });
        }
        /// <summary>
        /// Gets channel id from given channels user friendly name
        /// </summary>
        /// <param name="youtubeService">Youtube Service to use for id retrieval</param>
        /// <param name="username">Name of the channel</param>
        /// <returns>Returns the channel corresponding to username</returns>
        private Channel _GetChannel(YouTubeService youtubeService, string username)
        {
            var channel = youtubeService.Channels.List("contentDetails");
            channel.ForUsername = username;
            var channelList = channel.Execute().Items;
            if (channelList.Count == 0)
                throw new InvalidUsernameException();
            else
                return channelList.First();
        }
    }
}
