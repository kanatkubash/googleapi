﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// Reply from Translate API result
    /// </summary>
    public class TranslateReply
    {
        /// <summary>
        /// Translation of text
        /// </summary>
        public string Translation { get; set; }
        /// <summary>
        /// Source language detected from text. If source language is provided before translation, this is set to null
        /// </summary>
        public string SourceLanguage { get; set; }
    }
}
