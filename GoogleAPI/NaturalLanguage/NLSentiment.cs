﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI.NLReplyHelper
{
    /// <summary>
    /// Class that represents sentiment analysis results on given text
    /// </summary>
    public class NLSentiment
    {
        /// <summary>
        /// Polarity of sentiment given within range of [-1,1]. Positive values means positive sentiment
        /// </summary>
        public double? Polarity { get; set; }
        /// <summary>
        /// Magnitude of sentiment regardless of polarity
        /// </summary>
        public double? Magnitude { get; set; }
    }
}
