﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.GoogleAPI
{
    /// <summary>
    /// GmailReply object to represent unread message
    /// </summary>
    public class GmailReply
    {
        /// <summary>
        /// Id of the message
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Subject of the message
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// Text of message 
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Date of the message
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Sender of the message
        /// </summary>
        public string From { get; set; }
    }
}
